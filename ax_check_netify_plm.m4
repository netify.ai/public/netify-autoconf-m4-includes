dnl AX_CHECK_NETIFY_PLM
dnl -------------------
dnl
dnl Checks for sepcific Netify PLM version

AC_DEFUN([AX_CHECK_NETIFY_PLM], [
  AC_ARG_ENABLE([license-manager],
      [AS_HELP_STRING([--enable-license-manager], [Enable plugin license manager [default=yes]])],
      [],
      [enable_license_manager=yes])

  AS_IF([test "x$enable_license_manager" = "xyes"], [
      PKG_CHECK_MODULES([LIBNETIFY_PLM], [libnetify-plm = $NETIFY_PLM_MINVER])
      AC_DEFINE([_NPLM_ENABLE_LICENSE_MANAGER], [1],
          [Enable plugin license manager.])
  ])
])
