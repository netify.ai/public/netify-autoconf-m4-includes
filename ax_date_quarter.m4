dnl AX_RELEASE_QUARTER
dnl ------------------
dnl
dnl Return the start date of the current quarter.

AC_DEFUN([AX_RELEASE_QUARTER], [
  AC_SUBST([ND_RELEASE_QUARTER], [m4_esyscmd_s([
    printf "%04d-%02d-%02d\n" \
      "$(date '+%Y')" \
      "$((($(date '+%m') - 1) / 3 * 3 + 1))" \
      1
    ])
  ])
])
